trigger summitsAccTrigger on Account (before insert, after update, before update) { //after insert event not defined but trigger context variables isAfter() && isInsert() is used in if statement

    // Only run for North American Countries, we want to exclude international countries for now
    List<Account> northAmericanAcc = new List<Account>();
    getNorthAmericanAccs();//List returned from this method will give accounts whose billing countries are not part of north america 
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {  //better to consolidate (Trigger.isBefore && Trigger.isInsert) in one if statement 
                                 //no lines of code written to be executed in before Insert context
                                 //also else if statement will only be executed if Trigger.isInsert returns false
        } else if (Trigger.isUpdate) { //"else if" will only execute if the above if statement is false
            summitsAccTriggerHandler.beforeUpdate(northAmericanAcc);//null check for northAmericanAcc List should be done
                                                                    //for after update trigger.old, trigger.oldMap context variables can be leveraged to avoid calling this method whenever unnecessary
        }
    }
    if(Trigger.isAfter){  //better to consolidate (Trigger.isBefore && Trigger.isInsert) in one if statement 
        if (Trigger.isInsert) { 
            summitsAccTriggerHandler.afterInsert(northAmericanAcc);//else if statement will only be executed if Trigger.isInsert returns false
                                                                   //null check for northAmericanAcc List should be done
        } else if (Trigger.isUpdate) {
            summitsAccTriggerHandler.afterUpdate(northAmericanAcc);//null check for northAmericanAcc List should be done 
                                                                   //only after Update is executed always as after Insert event is not declared in the trigger context
                                                                   //also for after update trigger.old, trigger.oldMap context variables can be leveraged to avoid calling this method whenever unnecessary
        }
    }
    
    private static void getNorthAmericanAccs(){
        for(Account a : Trigger.new){
            if(a.BillingCountry != 'United States' || a.BillingCountry != 'Canada' || a.BillingCountry != 'Mexico'  ){ // replace '!=' operator by '=='
                northAmericanAcc.add(a);
            }
        }
    }
}