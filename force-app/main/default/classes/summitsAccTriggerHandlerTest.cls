@IsTest
global class summitsAccTriggerHandlerTest {//Handler class is only 48% covered, for code covergae it should be atleast 75%

    @TestSetup
    static void testSetup(){
        Integer count = 100;
        List<Account> accList = new List<Account>();
        for(Integer i = 0 ; i < 100 ; i++){
            Account temp = new Account(Name = 'Test Acc ' + i);
            accList.add(temp);
        }
        insert accList;

        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0 ; i < accList.size() ; i++){
            Contact temp = new Contact(LastName = 'Test Contact ' + i, AccountId = accList[i].Id);
            contactList.add(temp);
        }
        insert contactList;
    } //opp Test setup is missing

    @IsTest
    static void testWebsite(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId FROM Account];
        for(Account a : accList){
            a.RecordTypeId = summitsAccTriggerHandler.partnerRecordType;
            // replace above statement with this a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        }
        try {
            update accList;
        }catch (Exception e){
            System.debug(System.LoggingLevel.FINER, 'Exception: ' + e);
            //Assert will pass
            System.assert(e.getMessage().contains('This Account is a partner account and thus needs a website!'));//Assert statement is failing
        }
    }
    @IsTest
    static void testMatchingAddresses(){ //avoid failure of this test method by modifying getMismatchContacts method in the trigger handler
        List<Account> accList = [SELECT Id, Website, RecordTypeId, BillingCountry FROM Account];
        List<Contact> contactList = [SELECT Id, MailingCountry FROM Contact]; 

        //Assert will pass
        for(Contact a : contactList){
            System.assert(a.MailingCountry == null);
        }
        for(Account a : accList){
            a.BillingCountry = 'United States';//In the trigger context northAmericanAcc list entering into after update event will have accounts whose BillingCounty!='United States'
        }
        Test.startTest();
            update accList; 
        Test.stopTest();
        List<Contact> requeryContacts = [SELECT Id, MailingCountry FROM Contact];

        //Assert will pass
        for(Contact a : requeryContacts){
            System.assert(a.MailingCountry != null); //Assertion will fail as MailingCountry of Contacts are not updated
        }
    }
    @IsTest
    static void testERPmethod(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId, BillingCountry FROM Account];

        Test.startTest();
            summitsAccTriggerHandler.postAccount(accList[0]); //all callouts must be mocked using Apex WebMockServices
        Test.stopTest();
    } 
    //Test method missing which covers RectifyOpportunities method in the Trigger Handler. Also test data setup for opportunities is missing
}