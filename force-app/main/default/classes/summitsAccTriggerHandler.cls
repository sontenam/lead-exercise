public class summitsAccTriggerHandler {

    // Account Partner Record Type
    public static String partnerRecordType = '01221000001PIifAAG'; //Don't hard code record type Id. Use Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId() 
    public static final String username = 'thisIsMyUsername@test.com'; //Use an UserInfo utility class or UserInfo.getUserId() 
    public static final String password = 'passWord123'; //User's password cannot be explictly declared, it should be encrypted. It is desired this must configured in a custom setting or custom metadata
    
    public static void afterUpdate(List<Account> newAccounts) {
        alterContactsCountry(newAccounts);
        RectifyOpportunities(newAccounts);
    }

    public static void beforeUpdate(List<Account> newAccounts) {
        checkWebsite(newAccounts);
    }

    public static void afterInsert(List<Account> newAccounts) {
        sendAccountToERP(newAccounts); //after insert event not defined in account trigger. So this will not be executed
    }
    
    // Every Contact related to an account should have the same mailing country as its parent accounts billing country, if they do not match, update it, do SOQL in loop
    private static void alterContactsCountry(List<Account> newAccounts){
        List<Contact> contactUpdateList = new List<Contact>();
        for(Account a: newAccounts){
            contactUpdateList.addAll(getMismatchContacts(a, 'MailingCountry', 'BillingCountry'));//this method can make use of only one argument as last two arguments are always static
        }
        update contactUpdateList;//null check for contactUpdateList missing. Might throw dml exception
    }
    
    // Any account with the record type of partner should have a website, if not add an error for the end user
    public static void checkWebsite(List<Account> accList){
         for(Account a: accList)
             if(a.RecordType.Id == partnerRecordType && a.Website == null) //partnerRecordType Id must retrived by schema method
                 a.addError('This Account is a partner account and thus needs a website!');
    }

    public static List<Contact> getMismatchContacts(Account singleAccount, String firstValue, String secondValue){
        String qryString = 'SELECT Id, Name, MailingCountry FROM Contact WHERE ' + firstValue + '!=: +singleAccount.' + secondValue; //here firstValue and secondValue are always static //should leverage accountId on a single account to get contacts tied to a single account
        List<Contact> tempContacts = Database.query(qryString);// will return all contacts whose Mailing county is not equal to all account's Billing Country. Best practice is to query using a where clause with the account id 
        for(Contact a: tempContacts){ //Null check for tempContacts
            a.MailingCountry = singleAccount.BillingCountry;
        }
        return tempContacts;
    }

    // Sum up the $ amount of all opportunities for each account and override the annual revenue field on the account
    public static void RectifyOpportunities(List<Account> newAccounts){//this requirement can be done by creating a roll up summary(sum) field on account object 
        Map<Id, Decimal> accOpportunitiesMap = new Map<Id, Decimal>();
        List<Account> updateAccRevenue = new List<Account>();
        for(Opportunity a : [SELECT Id, AccountId, Amount FROM Opportunity WHERE AccountId IN: newAccounts]){ //'opp' variable in for loop is more appealing than 'a'
            Decimal oppValue = 0;
            if(accOpportunitiesMap.get(a.Account.Id) == null){
                accOpportunitiesMap.put(a.AccountId, a.Amount);
            }
            else{
                //null check for a.Amount in opprtunity is missing as opp Amount filed can be empty. This can also be accomodated by mofifying SOQL
                oppValue = accOpportunitiesMap.get(a.Account.Id);
                accOpportunitiesMap.put(a.AccountId, a.Amount + oppValue);
            }
        }
        for(Id a : accOpportunitiesMap.keySet()){//oppMap variable is more appealing than 'a'. Also 'a' is being used repetitively  
            Account acc = new Account(Id = a);
            acc.AnnualRevenue = accOpportunitiesMap.get(a);// A simple roll up summary field would eliminate code however a new custom field must be created on Account object
            updateAccRevenue.add(acc);
        }
        try {
            //null check for updateAccRevenue is missing. Actually a try block statement will suffix
            update updateAccRevenue;
        } catch(DmlException e) {
            //when an exception is caught a custom validation or toast notification is preferable on screen for users or simply use addError
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    

    // post all account to the external ERP system for billing purposes
    private static void sendAccountToERP(List<Account> accounts){
        for(Account a :accounts){
            HttpResponse res = postAccount(a);
            System.debug('The res ' + res);
            //once the post request is made, the response must be handled if any..please check the payload request
        }
    }
    //better to write callouts in another class. Also callouts with @future annotation must be used in trigger context
    @TestVisible
    private static HttpResponse postAccount(Account acc){//Recommended to use custom metadata or custom settings to configure callout settings like header, endpoint 
        String payload = JSON.serialize(acc);
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue); //for authentication try using OAuth and a connected App
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', authorizationHeader);
        req.setEndpoint('http://www.randomWebsite.com/');
        req.setMethod('POST');
        req.setBody(payload);
        HttpResponse res = h.send(req);
        return res;
    }
}